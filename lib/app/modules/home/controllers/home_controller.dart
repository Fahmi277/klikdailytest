import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  //TODO: Implement HomeController

  final selectedIndex = 0.obs;
  final listColor = [
    [Colors.green, Colors.white],
    [Colors.grey, Colors.black],
    [Colors.grey, Colors.black]
  ].obs;
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void onItemTapped(int index) => selectedIndex.value = index;
  void changeColor(int index) {
    if (index == 0) {
      listColor[0] = [Colors.green, Colors.white];
      listColor[1] = [Colors.grey, Colors.black];
      listColor[2] = [Colors.grey, Colors.black];
    }
    if (index == 1) {
      listColor[0] = [Colors.grey, Colors.black];
      listColor[1] = [Colors.green, Colors.white];
      listColor[2] = [Colors.grey, Colors.black];
    }
    if (index == 2) {
      listColor[0] = [Colors.grey, Colors.black];
      listColor[1] = [Colors.grey, Colors.black];
      listColor[2] = [Colors.green, Colors.white];
    }
  }
}
