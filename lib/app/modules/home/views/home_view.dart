// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static const List<Widget> _widgetOptions = <Widget>[
    Text(
      'Index 0: Home',
      style: optionStyle,
    ),
    Text(
      'Index 1: Business',
      style: optionStyle,
    ),
    Text(
      'Index 2: School',
      style: optionStyle,
    ),
    Text(
      'Index 3: Settings',
      style: optionStyle,
    ),
  ];

  // void _onItemTapped(int index) {
  //   setState(() {
  //     _selectedIndex = index;
  //   });
  // }
  @override
  Widget build(BuildContext context) {
    return Obx(() => Scaffold(
        body: SafeArea(
            child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    Text(
                      "Find Fresh Grocery",
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    CircleAvatar(
                      child: Icon(Icons.person),
                    )
                  ],
                ),
                SizedBox(height: 51),
                SizedBox(
                  height: 49,
                  child: TextFormField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.search),
                        hintText: "Search Groceries",
                        hintStyle: TextStyle(fontWeight: FontWeight.bold),
                        border: OutlineInputBorder(
                            borderRadius:
                                BorderRadius.all(Radius.circular(10)))),
                  ),
                ),
                SizedBox(
                  height: 21,
                ),
                Text(
                  "Categories",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 21,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    InkWell(
                      onTap: () {
                        controller.changeColor(0);
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: Container(
                          width: 98,
                          height: 39,
                          color: controller.listColor[0][0],
                          child: Center(
                              child: Text(
                            "Apple",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: controller.listColor[0][1]),
                          )),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        controller.changeColor(1);
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: Container(
                          width: 98,
                          height: 39,
                          color: controller.listColor[1][0],
                          child: Center(
                              child: Text(
                            "Orange",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: controller.listColor[1][1]),
                          )),
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () {
                        controller.changeColor(2);
                      },
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        child: Container(
                          width: 98,
                          height: 39,
                          color: controller.listColor[2][0],
                          child: Center(
                              child: Text(
                            "Banana",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: controller.listColor[2][1]),
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 17,
                ),
                ListItemDashboard(
                  itemName: "Apple Indonesia",
                  itemPrice: 34000,
                  itemStar: 4,
                ),
                ListItemDashboard(
                  itemName: "Apple Mandalika",
                  itemPrice: 34000,
                  itemStar: 4,
                ),
                ListItemDashboard(
                  itemName: "Apple Lokal Jawa",
                  itemPrice: 34000,
                  itemStar: 4,
                ),
                ListItemDashboard(
                  itemName: "Apple Brahman",
                  itemPrice: 34000,
                  itemStar: 4,
                ),
              ],
            ),
          ),
        )),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.green,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.shopping_bag),
              label: 'Cart',
              backgroundColor: Colors.green,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profile',
              backgroundColor: Colors.green,
            ),
          ],
          currentIndex: controller.selectedIndex.value,
          backgroundColor: Colors.green,
          selectedItemColor: Colors.white,
          onTap: controller.onItemTapped,
        )));
  }
}

class ListItemDashboard extends StatelessWidget {
  String itemName;
  int itemPrice;
  int itemStar;
  ListItemDashboard({
    Key? key,
    required this.itemName,
    required this.itemPrice,
    required this.itemStar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 105,
        width: double.infinity,
        child: Card(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Stack(
              children: [
                Center(
                    child: Row(
                  children: [
                    CircleAvatar(
                      child: Icon(Icons.person),
                    ),
                    SizedBox(width: 25),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          itemName,
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Text(
                          "Rp. $itemPrice /Kg",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Row(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                            Icon(
                              Icons.star,
                              color: Colors.amber,
                            ),
                          ],
                        )
                      ],
                    )
                  ],
                )),
                Positioned(
                    bottom: 0,
                    right: 0,
                    child: ElevatedButton(onPressed: () {}, child: Text("Buy")))
              ],
            ),
          ),
        ));
  }
}
